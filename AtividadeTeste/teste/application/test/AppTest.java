package application.test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class AppTest {
	
	@Test
	public void criarObjetos() {
		LeilaoTest.getInstance()
		.criarItem("Macbook Pro", 5.0000, true)
		.usuarioOferta("Gustavo")
		.criarLeilao(7000, true)
		.Leiloar();
		
		LeilaoTest.getInstance()
		.criarItem("Macbook Pro", 5.0000, true)
		.usuarioOferta("Ricardo")
		.criarLeilao(10000, true)
		.Leiloar();
		
		assertThat(LeilaoTest.getInstance().getLeilaoList().get(0), is(equalTo(LeilaoTest.getInstance().getLeilaoList().get(1))));
		//assertEquals(LeilaoTest.getInstance().getLeilaoList().get(1).getItem(), LeilaoTest.getInstance().getLeilaoList().get(0).getItem());
	}
	
	
	
}
