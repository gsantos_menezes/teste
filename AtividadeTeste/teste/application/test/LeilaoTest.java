package application.test;

import java.util.ArrayList;
import java.util.List;

import application.Bolha;
import application.bean.Item;
import application.bean.Leilao;
import application.bean.Usuario;

public class LeilaoTest {

	private static LeilaoTest instance;
	
	private Item item;
	private Usuario usuario;
	
	private List<Leilao> leilaoList = new ArrayList<Leilao>();
	
	public List<Leilao> getLeilaoList() {
		return leilaoList;
	}

	public void setLeilaoList(List<Leilao> leilaoList) {
		this.leilaoList = leilaoList;
	}

	public LeilaoTest Leiloar() {
		Bolha.getInstance().ordenar(leilaoList);
		return this;
	}
	
	public LeilaoTest criarItem(String nome, double preco, boolean ativo) {
		this.item = new Item(nome, preco, ativo);
		return this;
	}
	
	public LeilaoTest usuarioOferta(String nome) {
		this.usuario = new Usuario(nome);
		return this;
	}
	
	public LeilaoTest criarLeilao(double precoItemOferta, boolean ativo) {
		Leilao a = new Leilao(this.item, precoItemOferta, this.usuario, ativo);
		leilaoList.add(a);
		return this;
	}
	
	public static synchronized LeilaoTest getInstance() {
        if (instance == null)
            instance = new LeilaoTest();
 
        return instance;
    }
}
