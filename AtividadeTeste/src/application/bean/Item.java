package application.bean;

public class Item {

	private String nome;
	private double ofertaMinima;
	private boolean disponivel;
	
	public Item(String nome, double preco, boolean ativo) {
		this.nome = nome;
		this.ofertaMinima = preco;
		this.disponivel = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getOfertaMinima() {
		return ofertaMinima;
	}

	public void setOfertaMinima(double ofertaMinima) {
		this.ofertaMinima = ofertaMinima;
	}

	public boolean isDisponivel() {
		return disponivel;
	}

	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}
}
