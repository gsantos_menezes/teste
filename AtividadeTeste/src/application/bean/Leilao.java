package application.bean;

public class Leilao {
	
	private Item item;
	private double precoItemOferta;
	private Usuario usuario;
	private boolean ativo;

	public Leilao(Item item, double precoItemOferta, Usuario usuario, boolean ativo) {
		this.item = item;
		this.precoItemOferta = precoItemOferta;
		this.usuario = usuario;
		this.ativo = ativo;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public double getPrecoItemOferta() {
		return precoItemOferta;
	}

	public void setPrecoItemOferta(double precoItemOferta) {
		this.precoItemOferta = precoItemOferta;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	@Override
	public String toString() {
		return "Usu�rio: " + usuario.getNome() + " Oferta:" + precoItemOferta + " Item: " + item.getNome();
	}
}
