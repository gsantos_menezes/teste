package application;

import java.util.List;

import application.bean.Leilao;

public class Bolha {
	
	private static Bolha bolhinha;

	private int i = 0;
	private Leilao aux;

	public void ordenar(List<Leilao> leilaoList) {
		for(i=0;i<leilaoList.size();i++) {
			for (int j = 0; j < leilaoList.size(); j++) {
				if (leilaoList.get(i).getPrecoItemOferta() > leilaoList.get(j).getPrecoItemOferta()) {
					aux = leilaoList.get(i);
					leilaoList.set(i, leilaoList.get(j));
					leilaoList.set(j, aux);
				}
			}
		}
		System.out.println("Lista:");for(i=0;i<leilaoList.size();i++)
		System.out.println(" " + leilaoList.get(i).toString());
	}
	
	public static synchronized Bolha getInstance() {
        if (bolhinha == null)
            bolhinha = new Bolha();
 
        return bolhinha;
    }
}
